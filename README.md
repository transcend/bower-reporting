# Reporting Module
The "Reporting" module is a set of controls and functionality to provide the ability to view, download, and interact
with reports - exposed via a "Reporting Engine".

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-reporting.git
```

Other options:

*   * Download the code at [http://code.tsstools.com/bower-reporting/get/master.zip](http://code.tsstools.com/bower-reporting/get/master.zip)
*   * View the repository at [http://code.tsstools.com/bower-reporting](http://code.tsstools.com/bower-reporting)
*   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/reporting/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.reporting']);
```
## To Do
- Add additional unit tests
- Finish documentation