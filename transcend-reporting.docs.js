/**
 * @ngdoc overview
 * @name transcend.reporting
 * @description
 # Reporting Module
 The "Reporting" module is a set of controls and functionality to provide the ability to view, download, and interact
 with reports - exposed via a "Reporting Engine".

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-reporting.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-reporting/get/master.zip](http://code.tsstools.com/bower-reporting/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-reporting](http://code.tsstools.com/bower-reporting)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/reporting/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.reporting']);
 ```
 */
/**
 * @ngdoc object
 * @name transcend.reporting.reportingConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.reporting Report Module}. The "reportingConfig" object is a
 * {@link https://docs.angularjs.org/guide/module module constant} and can therefore be
 * overridden/configured by:
 *
 *  <pre>
 // Override the value of the 'reportingConfig' object.
 angular.module('myApp').value('reportingConfig', {
     url: '/api/',
     report: {
       url: 'http://localhost/MyWebApiApp'
     }
   });
 </pre>
 **/
/**
 * @ngdoc object
 * @name transcend.reporting.reportingConfig#resource
 * @propertyOf transcend.reporting.reportingConfig
 *
 * @description
 * Configuration object to configure common resource properties, such as the server URL and default parameters.
 * All resources in the "{@link transcend.reporting Reporting Module}" extend the resource config object first, and
 * then extend the resource's specific configuration. So you can set the default backend server URL for all
 * resources, by using this configuration, and define any other configurations that may vary as well.
 *
 * ## Configuration Properties
 *   * __url__ - a string that that represents the base url to the backend server.
 *   * __params__ - default URL parameters that will be passed to each service call.
 *
 <pre>
 angular.module('myApp').value('reportingConfig', {
       // The "Report" resource's URL will be set
       // by the resource url configuration.
       resource: {
         url: 'http://localhost/MyWebApiApplication',
         params: {}
       },
     });
 </pre>
 */
/**
 * @ngdoc directive
 * @name transcend.reporting.directive:reportList
 *
 * @description
 * The 'reportList' directive provides a user interface that displays a list of available reports for the current
 * application session.  One or more reports can be selected from the list and operated on using the features provided
 * in the Navigation Bar.
 *
 * @restrict EA
 * @element ANY
 * @replace true
 * @transclude true
 *
 * @requires transcend.reporting.Report
 * @requires transcend.$filter
 *
 * @param {Object} report The report selected by the user.
 * @param {function} paramFormatter The method used to format the report parameters for the report list.
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <report-list></report-list>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['ui.bootstrap', 'transcend.reporting', 'ngMockE2E'])
 .controller('Ctrl', function() {
  });
 </file>
 <file name="mocks.js">
 angular.module('app')
 .config(function(reportingConfig) {
   reportingConfig.reportList = {
     paramFormatter: function(param){
       if (param && param.name === 'TestParam'){
         param.value = 'Known Input';
         param.hidden = true;
       }
     }
   };
 })

 .run(function($httpBackend){
   var mocks = [{"name":"Straight Line Diagram","fileName":"ra","description":"Straight Line Diagram (SLD) report snapshot.","category":"ra","app":"","parameters":[{"name":"FromMeasure","type":"Double","value":0.0},{"name":"ToMeasure","type":"Double","value":0.0},{"name":"RepeatingImage","type":"Image"},{"name":"SignImage","type":"Image"},{"name":"RouteId","type":"String","value":""},{"name":"BarLegendImage","type":"Image"},{"name":"StickLegendImage","type":"Image"},{"name":"PageWidth","type":"Double","value":11.0},{"name":"PageHeight","type":"Double","value":8.5},{"name":"Grayscale","type":"Boolean","value":false},{"name":"Scale","type":"Double","value":0.0}]}, {"name":"Test Report (No Params)","fileName":"test","description":"Test report description here.","category":"test","app":"","parameters":[]}, {"name":"Test Report (Formatted Params)","fileName":"test","description":"Test report with formatted params.","category":"test2","app":"","parameters":[{"name":"TestParam","type":"String","value":""}]}];

   // Mock our HTTP calls.
   $httpBackend.whenGET(/api\/report/)
    .respond(angular.copy(mocks));
 })
 </file>
 </example>
 **/
/**
 * @ngdoc directive
 * @name transcend.reporting.directive:reportNavbar
 *
 * @description
 * The 'reportNavbar' directive provides a list of available features that operate on one or more selected report items.
 * If no reports are selected, these options are disabled in the user interface.
 *
 * @restrict EA
 * @element ANY
 * @replace true
 *
 * @requires transcend.reporting.Report
 * @requires ng.$window
 *
 * @param {array} reports The reports that have been selected by the user.
 * @param {object} search The search text used to filter the reports list.
 */
/**
 * @ngdoc directive
 * @name transcend.reporting.directive:reportParams
 *
 * @description
 * The 'reportParams' directive displays a list of attributes that are defined for a report item in the reports list.
 * A user can modify the values for these attributes to produce the desired report results.
 *
 * @restrict EA
 * @element ANY
 * @replace true
 *
 * @param {array} params The parameters and their values that were used to generate the report
 * @param {function} formatter The method used to format the report parameters for the report list.
 */
/**
 * @ngdoc service
 * @name transcend.reporting.service:Report
 *
 * @description
 * Provides a set of 'Report' utility functions that in turn invoke functions on the "Reporting Engine" used to create,
 * view and download reports using application data.
 * A {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} to interact with a "Reporting Engine"
 * entity for data reporting functionality  The purpose of this
 * {@link http://docs.angularjs.org/api/ngResource/service/$resource $resource} is to provide the necessary
 * functionality to create, view, edit and download reports.
 *
 * @requires $resource
 * @requires $window
 * @requires transcend.reporting.reportingConfig
 * @requires transcend.core.$notify
 *
 <h3>Calling Static Report Utility Functions</h3>
 <pre>
 var reports = Report.query();
 Report.downloadZip(exportType, reports);
 </pre>
 *
 * See {@link http://docs.angularjs.org/api/ngResource/service/$resource AngularJS's $resource} documentation for
 * details on what methods are natively available to a resource.
 */
/**
     * @ngdoc object
     * @name transcend.reporting.reportingConfig#report
     * @propertyOf transcend.reporting.reportingConfig
     *
     * @description
     * Configuration object for the {@link transcend.reporting.service:Report Service}.
     * This object provides the capability to override the service's default configuration.
     *
     * ## Configuration Properties
     *   * __url__ - the base path to the Reporting Engine program
     *   * __endpoint__ - the config file entry that contains the designed output filename
     *   * __designerEndpoint__ - the config file entry that contains the url to the Reporting Engine Editor
     *   * __viewerEndpoint__ - the config file entry that contains the url to the Reporting Engine Viewer
     *   * __params__ - any other attributes required for the requested operation, include:
     *      * __fileName__ - the name of the output file to which the report will be written
     *
     <pre>
     angular.module('myApp').value('securityConfig', {
       // This will use the url specified in the resource
       // configuration, and override the endpoints and
       // filename.
       Report: {
         url: '',
         endpoint: 'api/report/:fileName',
         designerEndpoint: 'reports/designer',
         viewerEndpoint: 'reports/viewer',
         params: {
           fileName: '@fileName'
     }
     });
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.reporting.service:Report#download
     * @methodOf transcend.reporting.service:Report
     *
     * @description
     * Downloads the reports list to the user's local computer. If only one report is present in the list, the report
     * is created using the href method, with the output path from the config file and the filename from the report.
     * If more than one report is present in the list, the downloadZip method is called, passing in the exportType and
     * reports list.
     *
     * @param {string|object} params Type of file to which the data is to be exported (i.e., CSV, PDF,...)
     * @param {array} reports The list of reports to be exported
     * @returns {object} The reference to the new window
     *
     * @example
     <pre>
     // Will export a single PDF file.
     Report.download({ fileType: 'pdf' }, report);

     // Will export a zip file with two PDF files in it.
     Report.download({ fileType: exportType }, [report1, report2]);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.reporting.service:Report#design
     * @methodOf transcend.reporting.service:Report
     *
     * @description
     * Opens the report designer window, passing in the Reporting Engine designer url and the list of reports.
     *
     * @param {object} report The report to be edited
     * @returns {object} The reference to the new window
     * @example
     <pre>
     Report.design(report);
     </pre>
     */
/**
     * @ngdoc method
     * @name transcend.reporting.service:Report#view
     * @methodOf transcend.reporting.service:Report
     *
     * @description
     * Opens the report viewer window, passing in the Reporting Engine view url and the list of reports.
     *
     * @param {object} report The report to be viewed
     * @returns {object} The reference to the new window
     * @example
     <pre>
     Report.view(report)
     </pre>
     */
